from __future__ import unicode_literals
import frappe
from frappe.utils import cint, format_datetime,add_days,today,date_diff,getdate,get_last_day,flt,nowdate
from frappe import throw, msgprint, _
from datetime import date
import re
import json
import traceback
import urllib
from urllib.request import urlopen
import requests

@frappe.whitelist()
def appErrorLog(title,error):
	d = frappe.get_doc({
			"doctype": "Custom Error Log",
			"title":str("User:")+str(title),
			"error":traceback.format_exc()
		})
	d = d.insert(ignore_permissions=True)
	return d


@frappe.whitelist()
def add_source_lead():
	try:
		lead_source=frappe.get_all("Lead Source",filters={"name":"India Mart"},fields=["*"])
		if len(lead_source)==0:
			doc=frappe.get_doc(dict(
				doctype="Lead Source",
				source_name="India Mart"
			)).insert(ignore_permissions=True)
			if doc:
				frappe.msgprint("India Mart Added As Lead Source")
		else:
			frappe.msgprint("India Mart Lead Source Available")
	except Exception as e:
		appErrorLog(frappe.session.user,e)

@frappe.whitelist()
def india_mart_tool_run(from_date,to_date):
	try:

		url=frappe.db.get_value("IndiaMart Setting","IndiaMart Setting","url")
		mobile=frappe.db.get_value("IndiaMart Setting","IndiaMart Setting","mobile_no")
		key=frappe.db.get_value("IndiaMart Setting","IndiaMart Setting","key")
		if not url:
			frappe.throw("URL Mandatory In IndiaMart Setting")
		if not mobile:
			frappe.throw("Mobile No Mandatory In IndiaMart Setting")
		if not key:
			frappe.throw("KEY Mandatory In IndiaMart Setting")
		req=str(url)+'GLUSR_MOBILE/'+str(mobile)+'/GLUSR_MOBILE_KEY/'+str(key)+'/Start_Time/'+str(from_date)+'/End_Time/'+str(to_date)+'/'
		res=requests.post(url=req)
		if res.text:
			count = 0
			for row in json.loads(res.text):
				if not row.get("Error_Message")==None:
					frappe.throw(row["Error_Message"])
				else:
					doc=add_lead(row["SENDERNAME"],row["SENDEREMAIL"],row["MOB"],row["SUBJECT"],row["QUERY_ID"])
					if doc:
						count += 1

			if count>=1:
				frappe.msgprint(str(count)+" Lead Created")

	except Exception as e:
		appErrorLog(frappe.session.user,e)

@frappe.whitelist()
def getLead():
	try:

		url=frappe.db.get_value("IndiaMart Setting","IndiaMart Setting","url")
		mobile=frappe.db.get_value("IndiaMart Setting","IndiaMart Setting","mobile_no")
		key=frappe.db.get_value("IndiaMart Setting","IndiaMart Setting","key")
		if not url:
			frappe.throw("URL Mandatory In IndiaMart Setting")
		if not mobile:
			frappe.throw("Mobile No Mandatory In IndiaMart Setting")
		if not key:
			frappe.throw("KEY Mandatory In IndiaMart Setting")
		req=str(url)+'GLUSR_MOBILE/'+str(mobile)+'/GLUSR_MOBILE_KEY/'+str(key)+'/Start_Time/'+str(date.today())+'/End_Time/'+str(date.today())+'/'
		res=requests.post(url=req)
		if res.text:
			count = 0
			for row in json.loads(res.text):
				if not row.get("Error_Message")==None:
					frappe.throw(row["Error_Message"])
				else:
					doc=add_lead(row["SENDERNAME"],row["SENDEREMAIL"],row["MOB"],row["SUBJECT"],row["QUERY_ID"])
					if doc:
						count += 1

			if count>=1:
				frappe.msgprint(str(count)+" Lead Created")

	except Exception as e:
		appErrorLog(frappe.session.user,e)


@frappe.whitelist()
def add_lead(name,email=None,mobile=None,requirement=None,query_id=None):
	try:
		lead_data=frappe.get_all("Lead",filters={"india_mart_id":query_id},fields=["*"])
		if len(lead_data)==0:
			doc=frappe.get_doc(dict(
				doctype="Lead",
				lead_name=name,
				email_address=email,
				phone=mobile,
				requirement=requirement,
				india_mart_id=query_id,
				source="India Mart"
			)).insert(ignore_permissions=True)
			return doc
	
	except Exception as e:
		appErrorLog(frappe.session.user,e)


